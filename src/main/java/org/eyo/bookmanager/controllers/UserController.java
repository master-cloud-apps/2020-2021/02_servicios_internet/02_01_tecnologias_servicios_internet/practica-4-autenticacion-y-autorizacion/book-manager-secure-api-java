package org.eyo.bookmanager.controllers;

import org.eyo.bookmanager.api.UserAPI;
import org.eyo.bookmanager.dtos.requests.UserRequestDTO;
import org.eyo.bookmanager.dtos.responses.UserCommentsResponseDTO;
import org.eyo.bookmanager.dtos.responses.UserResponseDTO;
import org.eyo.bookmanager.models.User;
import org.eyo.bookmanager.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping("/users")
public class UserController implements UserAPI {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("")
    public ResponseEntity<Void> createUSer(@RequestBody UserRequestDTO user) {
        if (this.userService.getUserByNick(user.getNick()) != null){
            return ResponseEntity.badRequest().build();
        }

        User userToCreate = new User(user.getNick(), user.getEmail(), user.getPassword());
        this.userService.save(userToCreate);

        URI location = fromCurrentRequest().path("/{id}").buildAndExpand(userToCreate.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("")
    public ResponseEntity<Collection<UserResponseDTO>> getUsers() {
        return ResponseEntity.ok(this.userService.getAll().stream().map(UserResponseDTO::new).collect(Collectors.toList()));
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> deleteUserById(@PathVariable Long userId) {
        User userToDelete = this.userService.findById(userId);

        if (userToDelete == null) {
            return ResponseEntity.notFound().build();
        }
        if (!userToDelete.getComments().isEmpty()){
            return ResponseEntity.badRequest().build();
        }
        this.userService.deleteById(userId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{userId}")
    public ResponseEntity<UserResponseDTO> updateUser(@PathVariable Long userId, @RequestBody UserRequestDTO updatedUser) {
        User userToUpdate = new User();
        userToUpdate.setEmail(updatedUser.getEmail());
        userToUpdate.setNick(updatedUser.getNick());


        if (this.userService.findById(userId) == null) {
            return ResponseEntity.notFound().build();
        }
        userToUpdate.setId(this.userService.findById(userId).getId());
        userToUpdate.setPassword(this.userService.findById(userId).getPassword());
        this.userService.save(userToUpdate);
        return ResponseEntity.ok().body(new UserResponseDTO(userToUpdate));
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserResponseDTO> getUserById(@PathVariable Long userId) {
        if (this.userService.findById(userId) == null) {
            return ResponseEntity.notFound().build();
        }
        User user = this.userService.findById(userId);
        UserResponseDTO response = new UserResponseDTO(user.getId(), user.getNick(), user.getEmail());
        return ResponseEntity.ok(response);
    }

    @Override
    @GetMapping("/{userId}/comments")
    public ResponseEntity<Collection<UserCommentsResponseDTO>> getUserComments(@PathVariable Long userId) {
        User user = this.userService.findById(userId);

        if (user == null){
            return ResponseEntity.notFound().build();
        }

        Collection<UserCommentsResponseDTO> outResponse = user.getComments().stream()
                .map(comment ->
                        new UserCommentsResponseDTO(comment.getBook().getId(), comment.getId(), comment.getContent(), comment.getPunctuation()))
                .collect(Collectors.toList());

        return ResponseEntity.ok(outResponse);
    }
}
