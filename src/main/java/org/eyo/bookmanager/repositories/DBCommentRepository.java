package org.eyo.bookmanager.repositories;

import org.eyo.bookmanager.models.Comment;
import org.eyo.bookmanager.repositories.db.CommentJpaRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
@Profile("db")
public class DBCommentRepository implements CommentRepository {

    private CommentJpaRepository commentJpaRepository;

    public DBCommentRepository(CommentJpaRepository commentJpaRepository) {
        this.commentJpaRepository = commentJpaRepository;
    }


    @Override
    public Collection<Comment> findAll(Long bookId) {
        return this.commentJpaRepository.findAll();
    }

    @Override
    public void save(Long bookId, Comment comment) {
        this.commentJpaRepository.save(comment);
    }

    @Override
    public void deleteById(Long commentId) {
        this.commentJpaRepository.deleteById(commentId);
    }

    @Override
    public Comment finById(Long commentId) {
        return this.commentJpaRepository.findById(commentId).orElse(null);
    }
}
