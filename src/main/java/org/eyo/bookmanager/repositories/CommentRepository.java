package org.eyo.bookmanager.repositories;

import org.eyo.bookmanager.models.Comment;

import java.util.Collection;

public interface CommentRepository {

    Collection<Comment> findAll(Long bookId);

    void save(Long bookId, Comment comment);

    void deleteById(Long commentId);

    Comment finById(Long commentId);
}
