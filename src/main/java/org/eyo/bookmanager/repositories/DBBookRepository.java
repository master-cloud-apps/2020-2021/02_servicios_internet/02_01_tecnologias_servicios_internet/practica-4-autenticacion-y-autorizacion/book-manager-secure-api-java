package org.eyo.bookmanager.repositories;

import org.eyo.bookmanager.models.Book;
import org.eyo.bookmanager.models.Comment;
import org.eyo.bookmanager.repositories.db.BookJpaRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
@Profile("db")
public class DBBookRepository implements BookRepository {

    private BookJpaRepository bookJpaRepository;

    public DBBookRepository(BookJpaRepository bookJpaRepository) {
        this.bookJpaRepository = bookJpaRepository;
    }

    @Override
    public Book save(Book book) {
        return this.bookJpaRepository.save(book);
    }

    @Override
    public Collection<Book> getAll() {
        return this.bookJpaRepository.findAll();
    }

    @Override
    public Book findById(Long bookId) {
        return this.bookJpaRepository.findById(bookId).orElse(null);
    }

    @Override
    public void deleteById(Long bookId) {
        this.bookJpaRepository.deleteById(bookId);
    }

    @Override
    public void addCommentToBook(Long bookId, Comment comment) {
        Book book = this.findById(bookId);
        book.getComments().add(comment);
        this.bookJpaRepository.save(book);
    }
}
