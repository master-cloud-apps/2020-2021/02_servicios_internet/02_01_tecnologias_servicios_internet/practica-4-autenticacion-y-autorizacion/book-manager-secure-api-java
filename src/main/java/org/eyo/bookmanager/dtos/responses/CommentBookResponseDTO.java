package org.eyo.bookmanager.dtos.responses;

import lombok.Getter;
import org.eyo.bookmanager.models.Comment;

@Getter
public class CommentBookResponseDTO {

    public CommentBookResponseDTO(Comment comment) {
        this.id = comment.getId();
        this.content = comment.getContent();
        this.nick = comment.getUser().getNick();
        this.email = comment.getUser().getEmail();
    }

    private Long id;
    private String content;
    private String nick;
    private String email;
}
