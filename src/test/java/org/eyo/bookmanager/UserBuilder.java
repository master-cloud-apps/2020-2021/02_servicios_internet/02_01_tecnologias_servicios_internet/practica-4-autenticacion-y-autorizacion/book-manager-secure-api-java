package org.eyo.bookmanager;

import org.eyo.bookmanager.models.User;

public class UserBuilder {

    private String nick;
    private String email;
    private String password = "testPass";

    public UserBuilder setNick(String nick) {
        this.nick = nick;
        return this;
    }

    public UserBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public User buildUser(){
        User user = new User();
        user.setEmail(this.email);
        user.setNick(this.nick);
        user.setPassword(this.password);
        return user;
    }

    public String build() {
        return "{" +
                "\"nick\": \"" + this.nick + "\"," +
                "\"password\": \"" + this.password+ "\"," +
                "\"email\": \"" + this.email + "\"" +
                "}";
    }
}
