package org.eyo.bookmanager.web.security;

import org.eyo.bookmanager.web.WebTest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class LoginTest extends WebTest {

    @Test
    void should_login() throws Exception {
        String login = "{" +
                "\"email\": \"" + email + "\"," +
                "\"password\": \"" + password + "\"" +
                "}";

        MvcResult result = this.mockMvc.perform(
                post("/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(login))
                .andExpect(status().isOk()).andReturn();

        assertNotNull(result.getResponse().getHeader("Authorization"));
    }

    @Test
    void should_not_login() throws Exception {
        String login = "{" +
                "\"email\": \"" + nick + "\"," +
                "\"password\": \"" + password + "\"" +
                "}";

        this.mockMvc.perform(
                post("/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(login))
                .andExpect(status().isUnauthorized());
    }
}
